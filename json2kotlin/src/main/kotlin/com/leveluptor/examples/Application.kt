package com.leveluptor.examples

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import javax.ws.rs.ApplicationPath
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Application
import javax.ws.rs.core.MediaType
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider


@ApplicationPath("api")
public class RestConfiguration : Application()


@Provider
public class ObjectMapperProvider : ContextResolver<ObjectMapper> {
    companion object {
        private val kotlinAwareMapper = ObjectMapper().registerModule(KotlinModule())

    }

    override fun getContext(type: Class<*>): ObjectMapper {
        return kotlinAwareMapper
    }
}


data class Book(val title: String, val pageCount: Int)


@Path("book")
public class BookController {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public fun createBook(book: Book): String {
        return book.toString()
    }

}
